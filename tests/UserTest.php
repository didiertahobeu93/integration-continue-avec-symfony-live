<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Users;

class UserTest extends TestCase
{
    

    public function testIsTrue(){
        $user = new Users();
        $date = new \DateTime;
        $user->setEmail('didier@sildysi.com');
        $user->setName('TAHOBEU Didier');
        $this->assertTrue($user->getName() === "TAHOBEU Didier");
        $this->assertTrue($user->getEmail() === "didier@sildysi.com");
    }

    public function testIsFalse(){
        $user = new Users();
        $user->setEmail('didier@sildysi.com');
        $user->setName('TAHOBEU Didier');

        $this->assertFalse($user->getName() === "Didier TAHOBEU");
        $this->assertFalse($user->getEmail() === "didier@sildysi.pro");
    }

    public function testIsEmpty(): void
    {
        $user = new Users();
        $this->assertEmpty($user->getName());
        $this->assertEmpty($user->getEmail());
    }
}
